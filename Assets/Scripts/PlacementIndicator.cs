﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlacementIndicator : MonoBehaviour
{
    public static PlacementIndicator Instance;
    
    public GameObject placementIndicator;
    public Camera arCamera;
    public TextMeshProUGUI instructionText;
    public GameObject scanIcon;
    public bool disabled;

    private ARRaycastManager _arRaycaster;
    private Pose _placementPose;
    private bool _placementPoseIsValid;
    private ARPlaneManager _planeManager;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    
    private void OnDestroy()
    {
        Instance = null;
    }

    private void Start()
    {
        _arRaycaster = FindObjectOfType<ARRaycastManager>();
        _planeManager = FindObjectOfType<ARPlaneManager>();
    }

    private void PlacementIndicatorController()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();

        void UpdatePlacementPose()
        {
            var center = new Vector3(Screen.width / 2f, Screen.height / 2f, 0f);
            var ray = arCamera.ScreenPointToRay(center);
            var hits = new List<ARRaycastHit>();

            if (_arRaycaster.Raycast(ray, hits, TrackableType.PlaneWithinPolygon))
            {
                _placementPose = hits[0].pose;
                _placementPoseIsValid = true;
                instructionText.text = "Choose a Surface";
            }

            if (!_placementPoseIsValid) return;

            var cameraTransform = Camera.current.transform;
            var cameraForward = cameraTransform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            _placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }

        void UpdatePlacementIndicator()
        {
            if (_placementPoseIsValid)
            {
                placementIndicator.SetActive(true);
                placementIndicator.transform.SetPositionAndRotation(_placementPose.position, _placementPose.rotation);
            }
            else
            {
                placementIndicator.SetActive(false);
            }
        }
    }

    private void Update()
    {
        if (disabled)
        {
            // placementIndicator.gameObject.SetActive(false);
            scanIcon.SetActive(false);
            return;
        }

        placementIndicator.gameObject.SetActive(true);
        PlacementIndicatorController();
        scanIcon.SetActive(_planeManager.trackables.count == 0);
    }
}