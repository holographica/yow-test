﻿using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    public static UIHandler Instance;

    public RectTransform questionTextRect;
    
    private ScreenOrientation _orientation;
    private CanvasScaler _canvasScaler;
    private readonly Vector2 _landscape = new Vector2(1920, 1080);
    private readonly Vector2 _portrait = new Vector2(1080, 1920);

    public Transform[] answerButtons3d;
    public Transform landscape3DAnswers;
    public Transform portrait3DAnswers;

    public Transform[] answerButtonsText;
    public Transform landscapeTextAnswersParent;
    public Transform portraitTextAnswersParent;
    public RectTransform[] answerButtonsTextIcons;
    public RectTransform[] answerButtonsTextQuestions;
    private readonly Vector2 _topPivot = new Vector2(0.5f, 1);
    private readonly Vector2 _topPos = new Vector2(0, -10f);
    private readonly Vector2 _leftPivot = new Vector2(0, 0.5f);
    private readonly Vector2 _leftPos = new Vector2(15, 0);

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
        
        _canvasScaler = FindObjectOfType<CanvasScaler>();
    }
    
    private void OnDestroy()
    {
        Instance = null;
    }

    private void Update()
    {
        _orientation = Screen.width > Screen.height ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
        switch (_orientation)
        {
            case ScreenOrientation.Landscape:
                questionTextRect.offsetMax = new Vector2(-250, -230);
                questionTextRect.offsetMin = new Vector2(400, 0);
                _canvasScaler.referenceResolution = _landscape;
                landscape3DAnswers.gameObject.SetActive(true);
                portrait3DAnswers.gameObject.SetActive(false);
                landscapeTextAnswersParent.gameObject.SetActive(true);
                portraitTextAnswersParent.gameObject.SetActive(false);
                foreach (var button in answerButtons3d) button.SetParent(landscape3DAnswers);
                SetTextAnswers(_orientation);
                break;
            case ScreenOrientation.Portrait:
                questionTextRect.offsetMax = new Vector2(-60, -230);
                questionTextRect.offsetMin = new Vector2(60, 0);
                _canvasScaler.referenceResolution = _portrait;
                landscape3DAnswers.gameObject.SetActive(false);
                portrait3DAnswers.gameObject.SetActive(true);
                landscapeTextAnswersParent.gameObject.SetActive(false);
                portraitTextAnswersParent.gameObject.SetActive(true);
                foreach (var button in answerButtons3d) button.SetParent(portrait3DAnswers);
                SetTextAnswers(_orientation);
                break;
        }

        void SetTextAnswers(ScreenOrientation orientation)
        {
            if (orientation == ScreenOrientation.Landscape)
            {
                foreach (var button in answerButtonsText)
                {
                    button.SetParent(landscapeTextAnswersParent);
                }

                foreach (var icon in answerButtonsTextIcons)
                {
                    icon.pivot = _leftPivot;
                    icon.anchorMax = _leftPivot;
                    icon.anchorMin = _leftPivot;
                    icon.anchoredPosition = _leftPos;
                }

                foreach (var question in answerButtonsTextQuestions)
                {
                    question.offsetMin = new Vector2(130, 0);
                    question.offsetMax = new Vector2(0, 0);
                }
            }
            else
            {
                foreach (var button in answerButtonsText)
                {
                    button.SetParent(portraitTextAnswersParent);
                }

                foreach (var icon in answerButtonsTextIcons)
                {
                    icon.pivot = _topPivot;
                    icon.anchorMax = _topPivot;
                    icon.anchorMin = _topPivot;
                    icon.anchoredPosition = _topPos;
                }

                foreach (var question in answerButtonsTextQuestions)
                {
                    question.offsetMin = new Vector2(0, 0);
                    question.offsetMax = new Vector2(0, -110);
                }
            }
        }
    }
}
