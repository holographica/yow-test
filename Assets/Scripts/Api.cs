﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class Api : MonoBehaviour
{
    public static Api Instance;

    public ExtendedGame Game;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public async Task<List<Question>> GetGame(string username)
    {
        // var startGameUrl = @"http://176.112.216.205/api/games/" + username;
        var startGameUrl = @"http://localhost:5000/api/games/" + username;
        var www = new UnityWebRequest {method = UnityWebRequest.kHttpVerbGET, url = startGameUrl};
        var dhJson = new DownloadHandlerBuffer();
        www.downloadHandler = dhJson;
        var result = www.SendWebRequest();
        while (!result.isDone) await Task.Yield();
        print(dhJson.text);
        Game = JsonConvert.DeserializeObject<ExtendedGame>(dhJson.text);
        return JsonConvert.DeserializeObject<List<Question>>(Game.GameQuestions);
    }
    
    public static async Task ReportGame(string answerString)
    {
        // const string reportGameUrl = @"http://176.112.216.205/api/answers/";
        const string reportGameUrl = @"http://localhost:5000/api/answers/";
        print(answerString);
        var form = new List<IMultipartFormSection> {new MultipartFormDataSection("data", answerString)};
        var www = UnityWebRequest.Post(reportGameUrl, form);
        // www.SetRequestHeader("Content-Type", "multipart/form-data");
        var result = www.SendWebRequest();
        while (!result.isDone) await Task.Yield();
        print(result.webRequest.downloadHandler.text);
    }
}

public class ExtendedGame
{
    public string ID;
    public string GameQuestions;
}

public enum QuestionTypes
{
    OnePlanet,
    FourPlanets
}

public class Question
{
    public string ID;
    public int Type;
    public string Title;
    public string Explanation;
    public int Answer;
    public string[] AnswerVariants;
    public string[] Planets; // this simulates addressables keys, for now it's regular prefabs

    // public QuestionOptions QuestionOptions;
}

// public class QuestionOptions
// {
//     public string Seconds { get; set; }
//     public bool AnsweredCorrectly { get; set; }
//     public int Order { get; set; }
// }

public class GameReport
{
    public string GameID;
    public List<Answer> Answers;
}

public class Answer
{
    public string QuestionID;
    public int Seconds;
    public bool IsCorrect;
    public int AnswerIndex;
}