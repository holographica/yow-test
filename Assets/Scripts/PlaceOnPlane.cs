using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
[RequireComponent(typeof(TryGetTouchPosition))]
public class PlaceOnPlane : MonoBehaviour
{
    [SerializeField] private GameObject ground;
    [SerializeField] private GameObject groundingScreen;
    [SerializeField] private GameObject menu;

    private bool _groundPlaced;
    
    private Pose _hitPose;
    private static readonly List<ARRaycastHit> Hits = new List<ARRaycastHit>();
    private ARRaycastManager _raycastManager;
    private ARPlaneManager _planeManager;

    private void Start()
    {
        _raycastManager = GetComponent<ARRaycastManager>();
        _planeManager = GetComponent<ARPlaneManager>();
    }

    private void Update()
    {
        if (_groundPlaced) return;
        if (!TryGetTouchPosition.Try(out var touchPosition)) return;
        PlaceGround();
            
        void PlaceGround()
        {
            if (!Application.isEditor)
            {
                if (!_raycastManager.Raycast(touchPosition, Hits, TrackableType.PlaneWithinPolygon)) return;
                _hitPose = Hits[0].pose;
                ground.gameObject.SetActive(true);
                var t = ground.transform;
                t.rotation = _hitPose.rotation;
                t.position = new Vector3(_hitPose.position.x, _hitPose.position.y + 0.01f, _hitPose.position.z);
                _planeManager.SetTrackablesActive(false);
                _planeManager.planePrefab = null;
            }
            else ground.gameObject.SetActive(true);

            PlacementIndicator.Instance.disabled = true;
            menu.SetActive(true);
            groundingScreen.SetActive(false);
            _groundPlaced = true;
        }
    }
}