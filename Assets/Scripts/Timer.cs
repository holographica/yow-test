﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public static Timer Instance;

    [SerializeField] private int timeLimit = 120; // seconds
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private Image fillClock;
    
    private float _timeLeft;

    private void Awake()
    {
        if (!ReferenceEquals(Instance, null) && Instance != this) Destroy(gameObject);
        else Instance = this;
    }
    
    private void OnDestroy()
    {
        Instance = null;
    }

    private void Start()
    {
        ResetTimer();
    }

    private void Update()
    {
        if (!Gameplay.Instance.isPlaying) return;
        GameTime();
    }

    public void ResetTimer()
    {
        _timeLeft = timeLimit;
        timerText.text = "00:00";
        fillClock.fillAmount = 0;
    }

    private void GameTime()
    {
        timerText.gameObject.SetActive(true);
        _timeLeft -= Time.deltaTime;
        var seconds = (int) _timeLeft % 60;
        var minutes = (int) _timeLeft / 60 % 60;
        var newText = $"{minutes:00}:{seconds:00}";
        timerText.text = newText;
        fillClock.fillAmount = 1 - _timeLeft / timeLimit;
        if (!(_timeLeft <= 0)) return;
        Gameplay.Instance.GameOver();
    }

    public float QuestionTime()
    {
        return timeLimit - _timeLeft;
    }
}