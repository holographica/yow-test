﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    [SerializeField] private float animationDuration;
    [SerializeField] private TMP_Text rightsText;
    [SerializeField] private TMP_Text wrongsText;
    [SerializeField] private TMP_Text questionNumberText;
    [SerializeField] private TMP_Text maxText;

    [SerializeField] private GameObject menuScreen;
    [SerializeField] private GameObject gameScreen;
    [SerializeField] private GameObject gameOverScreen;
    [SerializeField] private TMP_Text gameOverScoreText;
    [SerializeField] private TMP_Text gameOverTotalQuestionsText;

    [SerializeField] private TMP_Text questionText;

    [SerializeField] private GameObject fourPlanetsScreen;
    private readonly GameObject[] _fourPlanets = new GameObject[4];
    [SerializeField] private Transform[] fourPlanetsSpots;

    [SerializeField] private GameObject onePlanetScreen;
    private GameObject _onePlanet;
    private MeshRenderer _onePlanetsRenderer;
    [SerializeField] private TMP_Text[] onePlanetVariantsTexts;
    [SerializeField] private Transform onePlanetSpot;

    [SerializeField] private string[] planetsNames;
    [SerializeField] private GameObject[] planetsPrefabs;
    private readonly Dictionary<string, GameObject> _planetsAndMaterialsDict = new Dictionary<string, GameObject>();

    [HideInInspector] public bool isPlaying;

    private int _rightAnswers;
    private int _wrongAnswers;
    private const int MaxTotalQuestions = 4;
    private int _currentQuestionIndex;
    private Question _currentQuestion;

    // [SerializeField] private GameObject questionTextOverlay;

    private List<Question> _questions;
    private readonly List<Answer> _answers = new List<Answer>();

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        maxText.text = MaxTotalQuestions.ToString();
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    private void Start()
    {
        for (var i = 0; i < 8; i++)
        {
            _planetsAndMaterialsDict[planetsNames[i]] = planetsPrefabs[i];
        }
    }

    private void Update()
    {
        if (!isPlaying) return;
        rightsText.text = _rightAnswers.ToString();
        wrongsText.text = _wrongAnswers.ToString();
        questionNumberText.text = _currentQuestionIndex.ToString();
    }

    public async void StartGame()
    {
        Timer.Instance.ResetTimer();

        menuScreen.SetActive(false);
        gameScreen.SetActive(true);
        gameOverScreen.SetActive(false);
        isPlaying = true;
        _questions = await Api.Instance.GetGame("unnanego");
        NextQuestion();
    }

    private void NextQuestion()
    {
        fourPlanetsScreen.SetActive(false);
        onePlanetScreen.SetActive(false);

        // reset stuff after previous question
        if (_currentQuestionIndex != 0)
        {
            if (_currentQuestion.Type == (int) QuestionTypes.FourPlanets)
            {
                for (var i = 0; i < 4; i++)
                {
                    var planet = _fourPlanets[i].transform;
                    planet.DOMove(PlacementIndicator.Instance.placementIndicator.transform.position, animationDuration / 2);
                    planet.DOScale(Vector3.zero, animationDuration / 2);
                    Destroy(planet.gameObject, animationDuration);
                }
            }
            else
            {
                _onePlanet.transform.DOMove(PlacementIndicator.Instance.placementIndicator.transform.position, animationDuration / 2);
                _onePlanet.transform.DOScale(Vector3.zero, animationDuration / 2);
                Destroy(_onePlanet, animationDuration);
            }
        }

        // show next question
        _currentQuestion = _questions[_currentQuestionIndex++];

        if (_currentQuestion.Type == (int) QuestionTypes.FourPlanets)
        {
            questionText.text = _currentQuestion.Title;
            fourPlanetsScreen.SetActive(true);

            for (var i = 0; i < MaxTotalQuestions; i++)
            {
                _fourPlanets[i] = Instantiate(_planetsAndMaterialsDict[_currentQuestion.Planets[i]]);
                var planet = _fourPlanets[i].transform;
                planet.DOMove(fourPlanetsSpots[i].position, animationDuration);
                planet.DOScale(Vector3.one, animationDuration);
            }
        }
        else
        {
            questionText.text = _currentQuestion.Title;
            onePlanetScreen.SetActive(true);
            _onePlanet = Instantiate(_planetsAndMaterialsDict[_currentQuestion.Planets.First()]);
            _onePlanet.transform.rotation = PlacementIndicator.Instance.placementIndicator.transform.rotation;
            _onePlanet.transform.position = PlacementIndicator.Instance.placementIndicator.transform.position;
            _onePlanet.transform.DOMove(onePlanetSpot.position, animationDuration);
            _onePlanet.transform.DOScale(Vector3.one, animationDuration);

            for (var i = 0; i < MaxTotalQuestions; i++)
            {
                var answerVariant = _currentQuestion.AnswerVariants[i];
                onePlanetVariantsTexts[i].text = answerVariant;
            }
        }
    }

    public void Answer(int index)
    {
        print(_currentQuestion.Answer + " " + index);
        var answer = new Answer {Seconds = (int) Timer.Instance.QuestionTime(), AnswerIndex = index, IsCorrect = _currentQuestion.Answer == index, QuestionID = _currentQuestion.ID};
        if (answer.IsCorrect) _rightAnswers++;
        else _wrongAnswers++;
        _answers.Add(answer);

        if (_currentQuestionIndex == 4)
        {
            GameOver();
        }
        else
        {
            NextQuestion();
        }
    }

    public async void GameOver()
    {
        print("Game Over");
        isPlaying = false;
        gameScreen.SetActive(false);
        gameOverScreen.SetActive(true);
        gameOverScoreText.text = _rightAnswers.ToString();
        gameOverTotalQuestionsText.text = MaxTotalQuestions.ToString();
        _currentQuestion = null;
        _currentQuestionIndex = 0;

        var report = new GameReport {GameID = Api.Instance.Game.ID, Answers = _answers};
        await Api.ReportGame(JsonConvert.SerializeObject(report));
    }

    public void ToggleQuestionText()
    {
    }
}